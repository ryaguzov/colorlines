import javax.swing.*;
import javax.swing.JLabel;

import java.util.Random;

interface IBall
{
    void Set(int x, int y);
    void Destore();
    void Move(int x, int y);
} 

enum COLORS
{
    YELLOW,
    GREEN,
    BLUE,
    RED,
    ORANGE,
    PINK,
    BROWN;

    public static COLORS GetRandomColor() {
        Random random = new Random();
        return values()[random.nextInt(values().length)];
    }
}

class Ball implements IBall
{
    //static String _basePath = "src/pic/";
    private String _basePath = "P:\\develop\\projects\\java\\cursach\\pic\\";

    private int positionX, positionY;
    private ImageIcon _image;
    private COLORS _color;

    private int _width = 110;

    public JLabel label = new JLabel();

    public Ball(COLORS color)
    {
        _color = color;

        switch (_color)
        {
            case YELLOW:
                _image = new ImageIcon(_basePath + "yellowBall" + ".png");
                break;
            case GREEN:
                _image = new ImageIcon(_basePath + "greenBall.png");
                break;
            case BLUE:
                _image = new ImageIcon(_basePath + "blueBall.png");
                break;
            case RED:
                _image = new ImageIcon(_basePath + "redBall.png");
                break;
            case ORANGE:
                _image = new ImageIcon(_basePath + "orangeBall.png");
                break;
            case PINK:
                _image = new ImageIcon(_basePath + "pinkBall.png");
                break;
            case BROWN:
                _image = new ImageIcon(_basePath + "brownBall.png");
                break;
        }
        //_width = _image.getIconWidth();
        label.setIcon(_image);          
    }

    public Ball ( Ball copy )
    {
        this(copy._color);
        Set(copy.positionX, copy.positionY);
    }

    public void Set(int x, int y)
    {
        positionX = x;
        positionY = y;
        label.setBounds(positionX, positionY, _width, _width);

    }

    public void Destore()
    {
        //елси пять в ряд то исчезаем
    }

    public void Move(int x, int y)
    {
        label.setBounds(x, y, _width, _width);
        positionX = x;
        positionY = y;
        //label.setBounds(positionX + x, positionY + y, _width, _width);
    }

    public COLORS GetColor()
    {
        return _color;
    }

    public int GetX(){return positionX;}
    public int GetY(){return positionY;}
}
