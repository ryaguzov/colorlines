import javax.swing.*;
import javax.swing.JPanel;
import javax.swing.text.Position;

import com.sun.swing.internal.plaf.basic.resources.basic;

import GameFiled.FindPath.BallPosition;

import java.util.ArrayList;

import java.awt.Color;
import java.awt.List;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Random;

class GameFiled extends JPanel implements MouseListener
{
    //_workJPanel;
    private Ball positionGrid[][]; 
    private final int _gameFiledSize = 9;

    Ball selectedBall = null;
    // 0 - Не выбрано
    // 1 - Выбрано
    public int State = 0;

    public int Score = 0;

    public boolean LineFinded = false;
    private int lineLenth = 3;

    GameFiled()
    {
        addMouseListener(this);

        positionGrid = new Ball[_gameFiledSize][_gameFiledSize];
        setLayout(null);
        setBounds(180, 110, _gameFiledSize * 100, _gameFiledSize * 100);
        setVisible(true);
        setBackground( Color.gray );
        
    }

    class BallPosition
        {
            public int _X;
            public int _Y;
            public BallPosition prev = null;
            public BallPosition pointReachable[] = null;
            
            public BallPosition(int x, int y)
            {
                _X = x;
                _Y = y;
            }

            public void NextPoints()
            {
                pointReachable = new BallPosition[4];
                BallPosition up = null;
                BallPosition right = null;
                BallPosition down = null;
                BallPosition left = null;

                int nX = this._X;
                int nY = this._Y - 1;
                if ( this._Y < 1 && positionGrid[ nX ][ nY ] != null )
                {
                    up = new BallPosition( nX, nY );
                    up.prev = this;
                }

                nX = this._X + 1;
                nY = this._Y;
                if ( this._X < _gameFiledSize - 1 && positionGrid[ nX ][ nY ] != null )
                {
                    right = new BallPosition( nX, nY );
                    right.prev = this;
                }

                nX = this._X;
                nY = this._Y + 1;
                if ( this._Y < _gameFiledSize - 1 && positionGrid[ nX ][ nY ] != null )
                {
                    down = new BallPosition( nX, nY );
                    down.prev = this;
                }

                nX = this._X - 1;
                nY = this._Y;
                if ( this._X < 1 && positionGrid[ nX ][ nY ] != null )
                {
                    left = new BallPosition( nX, nY );
                    left.prev = this;
                }

                pointReachable[0] = up;
                pointReachable[1] = right; 
                pointReachable[2] = down;
                pointReachable[3] = left;
            }
        }

    private void RemoveBall(int x, int y)
    {
        this.remove(positionGrid[x][y].label);
        positionGrid[x][y] = null;
        this.repaint();
    }

    private void MoveBall(Ball selected, int positionX, int positionY)
    {
        if (positionGrid[ positionX ][ positionY ] != null )
        {
            System.out.println("Занято!");
            
            positionGrid[ selectedBall.GetX()/100 ][ selectedBall.GetY()/100 ].label.setBorder(null);
            positionGrid[ selectedBall.GetX()/100 ][ selectedBall.GetY()/100 ].Move(selectedBall.GetX(), selectedBall.GetY());

            return;
        }

        FindPath pathFinder = new FindPath(selected.GetX()/100, selected.GetY()/100, positionX, positionY);
        
        ArrayList<BallPosition> path = pathFinder.Run();

        Ball prevBall = selected;
        for (int i = path.size() - 2; i >= 0; i--)
        {
            BallPosition currentPos = path.get(i);
            positionGrid[ currentPos._X ][ currentPos._Y ] = new Ball( prevBall );
            
            RemoveBall(prevBall.GetX()/100,prevBall.GetY()/100);

            positionGrid[ currentPos._X ][ currentPos._Y ].Move(currentPos._X * 100, currentPos._Y  * 100);                    
            this.add(positionGrid[ currentPos._X ][ currentPos._Y ].label);
            prevBall = positionGrid[ currentPos._X ][ currentPos._Y ];
        }
    }

    public int CountArray()
    {
        int size = 0;
        for(int i = 0; i < _gameFiledSize; i++)
        {
            for(int j = 0; j < _gameFiledSize; j++)
            {
                if (positionGrid[i][j] != null) 
                {
                    size++;
                }
            }
        }
        return size;
    }
    
    // 0 - Игра продолжается
    // 1 - Игра окончена
    public int NewStage( Ball nextBalls[])
    {
        Random random_X = new Random();
        Random random_Y = new Random();

        for (int i = 0; i < 3; i++)
        {  
            int tmp = CountArray();
            if( tmp == 81)
            {
                System.out.println("Full");
                return 1;
            }
            
            int PositonX = random_X.nextInt(_gameFiledSize);
            int PositonY = random_Y.nextInt(_gameFiledSize);

            while (positionGrid[ PositonX ][ PositonY ] != null)
            {
                PositonX = random_X.nextInt(_gameFiledSize);
                PositonY = random_Y.nextInt(_gameFiledSize);
            }
        
            positionGrid[ PositonX ][ PositonY ] = nextBalls[i];
            positionGrid[ PositonX ][ PositonY ].Set( PositonX * 100, PositonY * 100);
            System.out.println(PositonX + " " + PositonY + " color " + nextBalls[i].GetColor());
            this.add( positionGrid[ PositonX ][ PositonY ].label );
        }
        System.out.println("-----------------");
        FindLine();

        return 0;
    }

    private boolean FindLineY()
    {        
        ArrayList<Ball> sameColor = new ArrayList<Ball>();        

        boolean readyToDelete = false;
        boolean finded = false;

        for(int y = 0; y < _gameFiledSize; y++)
        {
            if (positionGrid[0][y] != null )
                sameColor.add(new Ball(positionGrid[0][y]));            

            for(int x = 1; x < _gameFiledSize; x++)
            {            
                boolean currentColorNOTNull = positionGrid[x][y] != null;
                boolean prevColorExists = sameColor.size() > 0;
                boolean colorAreEq = false;

                if (currentColorNOTNull && prevColorExists)                 
                    colorAreEq = positionGrid[x][y].GetColor() == sameColor.get(sameColor.size()-1).GetColor();                
                    
                if ( colorAreEq )
                {
                    sameColor.add(new Ball(positionGrid[x][y]));
                }
                else 
                {
                    if ( readyToDelete )
                    {
                        int size = sameColor.size();
                        for ( int i = 0; i < size; i ++)                        
                        {
                            RemoveBall(sameColor.get(i).GetX()/100, sameColor.get(i).GetY()/100);
                            Score++;
                        }
                        finded = true;
                        readyToDelete = false;
                        sameColor.clear();
                    }
                    else
                    {
                        sameColor.clear();
                        if (currentColorNOTNull)
                        {                            
                            sameColor.add(positionGrid[x][y]);
                        }
                    }
                }
                                
                if (sameColor.size() >= lineLenth) 
                    readyToDelete = true;                               
            }            

            if ( readyToDelete )
            {
                int size = sameColor.size();
                for ( int i = 0; i < size-1; i ++)                        
                {
                    RemoveBall(sameColor.get(i).GetX()/100, sameColor.get(i).GetY()/100);
                    Score++;
                }
                finded = true;
                readyToDelete = false;
            }
            sameColor.clear();
        }
        LineFinded = finded;
        return finded;      
    }
    private boolean FindLineX()
    {        
        ArrayList<Ball> sameColor = new ArrayList<Ball>();        

        boolean readyToDelete = false;
        boolean finded = false;

        for(int x = 0; x < _gameFiledSize; x++)
        {
            if (positionGrid[x][0] != null )
                sameColor.add(new Ball(positionGrid[x][0]));            

            for(int y = 1; y < _gameFiledSize; y++)
            {            
                boolean currentColorNOTNull = positionGrid[x][y] != null;
                boolean prevColorExists = sameColor.size() > 0;
                boolean colorAreEq = false;

                if (currentColorNOTNull && prevColorExists)                 
                    colorAreEq = positionGrid[x][y].GetColor() == sameColor.get(sameColor.size()-1).GetColor();                
                    
                if ( colorAreEq )
                {
                    sameColor.add(new Ball(positionGrid[x][y]));
                }
                else 
                {
                    if ( readyToDelete )
                    {
                        int size = sameColor.size();
                        for ( int i = 0; i < size; i ++)                        
                        {
                            RemoveBall(sameColor.get(i).GetX()/100, sameColor.get(i).GetY()/100);
                            Score++;
                        }
                        finded = true;
                        readyToDelete = false;
                        sameColor.clear();
                    }
                    else
                    {
                        sameColor.clear();
                        if (currentColorNOTNull)
                        {
                            sameColor.add(positionGrid[x][y]);
                        }
                    }
                }
                                
                if (sameColor.size() >= lineLenth) 
                    readyToDelete = true;                               
            }            

            if ( readyToDelete )
            {
                int size = sameColor.size();
                for ( int i = 0; i < size-1; i ++)                        
                {
                    RemoveBall(sameColor.get(i).GetX()/100, sameColor.get(i).GetY()/100);
                    Score++;
                }
                finded = true;
                readyToDelete = false;
            }
            sameColor.clear();
        }
        LineFinded = finded;
        return finded;      
    }
    
    public boolean FindLine()
    {
        return FindLineX() || FindLineY();
    }
    
    public void Update()
    {
        this.repaint();
    }

    public void Clear()
    {
        this.removeAll();
    }

    // State = 0 Шар не выбран => выбираем шар
    // State = 1 Шар выбран => выбираем точку
    // State = 3 Позиция шара = выбиранной позиции => выбираем другой шар
    public void mouseClicked(MouseEvent clicked)
    {
        if (State == 0 || State == 3)
        {            
            int positionX = clicked.getX()/100;
            int positionY = clicked.getY()/100;

            boolean tt = positionGrid[ positionX ][ positionY ] != null;
            if ( tt )
            {
                State = 1;                
                selectedBall = new Ball(positionGrid[ positionX ][ positionY ]);
                
                positionGrid[ positionX ][ positionY ].label.setBorder(BorderFactory.createMatteBorder( 5, 5, 5, 5, Color.red));
                positionGrid[ positionX ][ positionY ].Move(selectedBall.GetX()-5, selectedBall.GetY());

                this.setBorder(BorderFactory.createMatteBorder( 15, 15, 15, 15, Color.red));
                return;
            }
            State = 0;
            LineFinded = false;
            System.out.println(positionX + " " + positionY);

        }
        if (State == 1)
        {
            this.setBorder(null);

            int positionX = clicked.getX()/100;
            int positionY = clicked.getY()/100;

            if (selectedBall.GetX()/100 != positionX ||
                selectedBall.GetY()/100 != positionY)
            {
                MoveBall(selectedBall, positionX, positionY);
                FindLine();
                State = 0;
            }
            else{
                positionGrid[ selectedBall.GetX()/100 ][ selectedBall.GetY()/100 ].label.setBorder(null);
                positionGrid[ selectedBall.GetX()/100 ][ selectedBall.GetY()/100 ].Move(selectedBall.GetX(), selectedBall.GetY());
                State = 3;
            }
            selectedBall = null;            
            
        }
    }

    public void mouseEntered(MouseEvent e){};
    public void mouseExited(MouseEvent e){};
    public void mousePressed(MouseEvent e){};
    public void mouseReleased(MouseEvent e){};


    class FindPath
    {
        ArrayList<BallPosition> explored = new ArrayList<BallPosition>();
        ArrayList<BallPosition> reachable = new ArrayList<BallPosition>();
        ArrayList<BallPosition> avalible = new ArrayList<BallPosition>();
        ArrayList<BallPosition> path = null;
        BallPosition endPositon;

        public FindPath(int startX, int startY, int endX, int endY)
        {
            Cast();
            explored.add(new BallPosition(startX, startY));
            endPositon = new BallPosition(endX, endY);
                
        }

        public ArrayList<BallPosition> Run()
        {
            //while( avalible.isEmpty() == false )
            //{
                for(int j = 0; j < explored.size(); j++)
                {   
                    explored.get(j).NextPoints();
                    for(int i = 0; i < 4; i++)
                    {                    
                        if ( explored.get(j).pointReachable[i] != null )
                        {   
                            explored.add(explored.get(j).pointReachable[i]);
                            avalible.remove(explored.get(j).pointReachable[i]);
                            explored.remove(j);
                        }

                        if ( explored.get(j).pointReachable[i] == endPositon )
                        {
                            BuildPath(endPositon);
                            break;
                        }
                    }
                }
            //}
            return path;
        }

        private void Cast()
        {
            for(int x = 0; x < _gameFiledSize; x++)
                for(int y = 0; y < _gameFiledSize; y++)
                {
                    if (positionGrid[x][y] == null)
                        avalible.add(new BallPosition(x, y));
                }
        }
        
        private void BuildPath( BallPosition endPositon )
        {
            path = new ArrayList<BallPosition>();
            while(endPositon.prev != null)
            {
                path.add(endPositon);
                endPositon = endPositon.prev;
            }
        }

        }
}